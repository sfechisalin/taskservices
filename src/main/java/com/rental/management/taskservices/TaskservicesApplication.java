package com.rental.management.taskservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskservicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaskservicesApplication.class, args);
	}

}

