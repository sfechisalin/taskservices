package com.rental.management.taskservices.service;

import com.rental.management.taskservices.model.Task;
import com.rental.management.taskservices.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Transactional
    public List<Task> getByAssigner(String assignee){
        return taskRepository.findByAssignee(assignee);
    }

    @Transactional
    public void addNewTask(Task task){
        taskRepository.save(task);
    }

    @Transactional
    public void deleteTask(String assignee, String assigner, String title){
        taskRepository.deleteByAssigneeAndAssignerAndTitle(assignee, assigner, title);
    }
}
