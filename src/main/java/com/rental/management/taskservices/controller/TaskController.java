package com.rental.management.taskservices.controller;

import com.rental.management.taskservices.model.Task;
import com.rental.management.taskservices.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
public class TaskController {
    @Autowired
    private TaskService taskService;

    @GetMapping("/by-Assignee")
    @ResponseStatus(HttpStatus.OK)
    public List<Task> getByAssigner(@RequestParam("assigner") String assigner){
        return taskService.getByAssigner(assigner);
    }

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public void addTask(@RequestBody Task task){
        System.out.println("addtask");
        System.out.println(task);
        taskService.addNewTask(task);
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.OK)
    public void deleteTask(@RequestParam("assignee") String assignee, @RequestParam("assigner") String assigner, @RequestParam("title") String title){
        System.out.println(assignee + " " + assigner + " " + title);
        taskService.deleteTask(assignee, assigner, title);
    }

}
