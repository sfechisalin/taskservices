package com.rental.management.taskservices.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    @Column(name="assignee")
    private String assignee;


    @Column(name="assigner")
    String assigner;

    @Column(name="title")
    String title;

    @Column(name="Description")
    String description;

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", assignee='" + assignee + '\'' +
                ", assigner='" + assigner + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
